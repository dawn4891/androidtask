package com.dawnsoft.dawn.androidtask;

/**
 * Created by dawn on 3/30/2018.
 */

public class StatusData {
    private String user;
    private String status;
    private String statusID;
    private String statusDate;
    private String likes;

    public StatusData(String user, String status, String statusID, String likes, String statusDate) {
        this.user = user;
        this.status = status;
        this.statusID = statusID;
        this.likes = likes;
        this.statusDate = statusDate;
    }

    public String getUser() {
        return user;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusID() {
        return statusID;
    }

    public String getLikes() {
        return likes;
    }

    public String getStatusDate() {
        return statusDate;
    }
}
