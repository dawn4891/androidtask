package com.dawnsoft.dawn.androidtask.loginfrags;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by dawn on 4/2/2018.
 */

public class LoginFragmentPagerAdapter extends FragmentPagerAdapter {

    public LoginFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new SignInFragment();
        }else return new SignUpFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }
}
