package com.dawnsoft.dawn.androidtask;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by dawn on 3/30/2018.
 */

public class StatusDataAdapter extends ArrayAdapter<StatusData>{
    public StatusDataAdapter(@NonNull Context context, @NonNull List<StatusData> objects) {
        super(context, 0, objects);

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
        View view = convertView;
        if (view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.item_status_list, parent, false);
        }

        StatusData currentStatus = getItem(position);

        TextView userName = (TextView)view.findViewById(R.id.user_name);
        assert currentStatus != null;
        userName.setText(currentStatus.getUser());
        TextView statusDate = (TextView)view.findViewById(R.id.status_date);
        statusDate.setText(String.valueOf(currentStatus.getStatusDate()));
        TextView status = (TextView)view.findViewById(R.id.user_status);
        status.setText(currentStatus.getStatus());
        TextView likeCounter = (TextView)view.findViewById(R.id.like_counter);
        likeCounter.setText(currentStatus.getLikes());

        LinearLayout like_view = (LinearLayout)view.findViewById(R.id.like_view);

        ImageView item_menu = (ImageView)view.findViewById(R.id.item_menu);
        item_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, ((ListView) parent).getAdapter().getItemId(position));
            }
        });
        like_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, ((ListView) parent).getAdapter().getItemId(position));
            }
        });


        return view;
    }

}
