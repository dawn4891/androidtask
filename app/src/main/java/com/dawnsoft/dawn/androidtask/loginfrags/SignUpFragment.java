package com.dawnsoft.dawn.androidtask.loginfrags;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dawnsoft.dawn.androidtask.HomeActivity;
import com.dawnsoft.dawn.androidtask.R;
import com.dawnsoft.dawn.androidtask.app.AppConfig;
import com.dawnsoft.dawn.androidtask.app.AppController;
import com.dawnsoft.dawn.androidtask.helper.SQLiteHandler;
import com.dawnsoft.dawn.androidtask.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

//    TextInputEditText name, address, ph_number, username, password, confirmPassword;

    private SessionManager sessionManager;
    private SQLiteHandler db;


    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_sign_up, container, false);

        sessionManager = new SessionManager(getContext());
        db = new SQLiteHandler(getContext());

        final TextInputEditText name = (TextInputEditText)view.findViewById(R.id.su_name);
        final TextInputEditText address = (TextInputEditText)view.findViewById(R.id.su_address);
        final TextInputEditText ph_number = (TextInputEditText)view.findViewById(R.id.su_ph_number);
        final TextInputEditText username = (TextInputEditText)view.findViewById(R.id.su_username);
        final TextInputEditText password = (TextInputEditText)view.findViewById(R.id.su_password);
        final TextInputEditText confirmPassword = (TextInputEditText)view.findViewById(R.id.su_confirm_password);
        Button btnRegister = (Button)view.findViewById(R.id.su_register);

        //check if user is already logged in or not
        if (sessionManager.isLoggedIn()){
            Intent chkIntent = new Intent(getActivity(), HomeActivity.class);
            chkIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(chkIntent);
        }

        //register button click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check for the empty fields
                if (!name.getText().toString().isEmpty() && !address.getText().toString().isEmpty()
                && !ph_number.getText().toString().isEmpty() && !username.getText().toString().isEmpty()
                && !password.getText().toString().isEmpty() && !confirmPassword.getText().toString().isEmpty()){

                    //check for both password match
                    if (password.getText().toString().equals(confirmPassword.getText().toString())){
                        //calling the register user
                        registerUser(name.getText().toString(), address.getText().toString(),
                                ph_number.getText().toString(), username.getText().toString(), password.getText().toString());
                    } else Toast.makeText(getContext(), "Password missmatch!!!", Toast.LENGTH_SHORT).show();
                } else Toast.makeText(getContext(), "Missing fields.", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    //function to store user in mysql db
    private void registerUser(final String name, final String address, final String ph_number,
                              final String username, final String password){
        //tag to cancel the request
        String tag_string_req = "req_register";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean error = jsonObject.getBoolean("error");
                            if (!error) {

                                sessionManager.setLogin(true);
                                String name = jsonObject.getString("name");
                                String username = jsonObject.getString("username");
                                String user_id = jsonObject.getString("user_id");
                                String created_date = jsonObject.getString("user_created");
                                db.addUsers(name, username, user_id, created_date);

                                //start activity
                                Intent registerIntent = new Intent(getActivity(), HomeActivity.class);
                                registerIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(registerIntent);

                                Toast.makeText(getContext(), "User registration successful.", Toast.LENGTH_SHORT).show();

                            } else {
                                String errorMsg = jsonObject.getString("error_message");
                                Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("address", address);
                params.put("ph_number", ph_number);
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };

        //adding request to the request queue
        AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
    }

}
