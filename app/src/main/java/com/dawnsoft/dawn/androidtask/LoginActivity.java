package com.dawnsoft.dawn.androidtask;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dawnsoft.dawn.androidtask.app.AppConfig;
import com.dawnsoft.dawn.androidtask.app.AppController;
import com.dawnsoft.dawn.androidtask.helper.SQLiteHandler;
import com.dawnsoft.dawn.androidtask.helper.SessionManager;
import com.dawnsoft.dawn.androidtask.loginfrags.LoginFragmentPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity{

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //hiding actionbar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        ViewPager loginViewpager = (ViewPager)findViewById(R.id.login_viewpager);
        LoginFragmentPagerAdapter fragmentPagerAdapter = new LoginFragmentPagerAdapter(getSupportFragmentManager());
        loginViewpager.setAdapter(fragmentPagerAdapter);


    }

}
