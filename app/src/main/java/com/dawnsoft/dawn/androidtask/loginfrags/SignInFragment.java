package com.dawnsoft.dawn.androidtask.loginfrags;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dawnsoft.dawn.androidtask.HomeActivity;
import com.dawnsoft.dawn.androidtask.LoginActivity;
import com.dawnsoft.dawn.androidtask.R;
import com.dawnsoft.dawn.androidtask.app.AppConfig;
import com.dawnsoft.dawn.androidtask.app.AppController;
import com.dawnsoft.dawn.androidtask.helper.SQLiteHandler;
import com.dawnsoft.dawn.androidtask.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment {

    TextInputEditText inputUsername;
    TextInputEditText inputPassword;
    private SessionManager sessionManager;
    private SQLiteHandler db;


    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        db = new SQLiteHandler(getContext());
        sessionManager = new SessionManager(getContext());

        if (sessionManager.isLoggedIn()){
            Intent loginIntent = new Intent(getActivity(), HomeActivity.class);
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginIntent);
        }
        //initializing different views
        inputUsername = (TextInputEditText) view.findViewById(R.id.username_SA);
        inputPassword = (TextInputEditText) view.findViewById(R.id.password_SA);
        Button btnLogin = (Button)view.findViewById(R.id.btn_login_SA);

        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!inputUsername.getText().toString().isEmpty() && !inputPassword.getText().toString().isEmpty()){
                    checkLogin(inputUsername.getText().toString(), inputPassword.getText().toString());
                } else {
                    Toast.makeText(getActivity(), " Missing fields!!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void checkLogin(final String username, final String password){

        // Tag used to cancel the request
        String tag_string_req = "req_login";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean error = jsonObject.getBoolean("error");
                            if (!error) {
                                sessionManager.setLogin(true);
                                String name = jsonObject.getString("name");
                                String username = jsonObject.getString("username");
                                String user_id = jsonObject.getString("user_id");
                                String created_date = jsonObject.getString("user_created");
                                db.addUsers(name, username, user_id, created_date);

                                // Launch main activity
                                Intent intent = new Intent(getActivity(),
                                        HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                // Error in login. Get the error message
                                String errorMsg = jsonObject.getString("error_message");
                                Toast.makeText(getContext(),
                                        errorMsg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getParams() {

                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
    }

}
