package com.dawnsoft.dawn.androidtask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dawnsoft.dawn.androidtask.app.AppConfig;
import com.dawnsoft.dawn.androidtask.app.AppController;
import com.dawnsoft.dawn.androidtask.helper.SQLiteHandler;
import com.dawnsoft.dawn.androidtask.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    SQLiteHandler sqLiteHandler;
    StatusDataAdapter adapter;

    ArrayList<StatusData> statusData;

    ProgressBar progressBar;
    EditText statusET;

    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sessionManager = new SessionManager(getApplicationContext());
        statusData = new ArrayList<StatusData>();

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        sqLiteHandler = new SQLiteHandler(getApplicationContext());
        final String user_id = String.valueOf(sqLiteHandler.getUserId());

        adapter = new StatusDataAdapter(this, statusData);
        final ListView statusList = (ListView)findViewById(R.id.statusList);
        //finally setting adapter
        statusList.setAdapter(adapter);

        final SwipeRefreshLayout swipeRefresh = (SwipeRefreshLayout)findViewById(R.id.swiperefresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStatus();
                statusList.setAdapter(adapter);
                swipeRefresh.setRefreshing(false);
            }
        });

        //getting all the statuses and putting them in a list
        getStatus();

        //for posting status
        statusET = (EditText)findViewById(R.id.et_status);
        ImageView btnPost = (ImageView)findViewById(R.id.btn_post);

        //on click event of post button
        btnPost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!statusET.getText().toString().isEmpty()){
                    createStatus(statusET.getText().toString(), user_id);
                    statusList.invalidateViews();
                    getStatus();
                    statusList.setAdapter(adapter);
                }
                statusET.setText(null);

            }
        });

        //like button listener
        statusList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewID = view.getId();

                if (viewID == R.id.like_view){
//                    Toast.makeText(getApplicationContext(), "like button clicked at position " + position, Toast.LENGTH_SHORT).show();
                    String statusID = adapter.getItem(position).getStatusID();
                    likeButton(statusID);
                    adapter.notifyDataSetChanged();

                } if (viewID == R.id.item_menu){
                    PopupMenu popupMenu = new PopupMenu(getApplicationContext(), view);
                    getMenuInflater().inflate(R.menu.context_menu, popupMenu.getMenu());

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()){
                                case R.id.menu_update:
                                    Toast.makeText(getApplicationContext(), "Update menu clicked", Toast.LENGTH_SHORT).show();
                                    return true;
                                case R.id.menu_delete:
                                    Toast.makeText(getApplicationContext(), "Delete menu clicked", Toast.LENGTH_SHORT).show();
                                    return true;
                            }
                            return false;
                        }
                    });

                    popupMenu.show();
                }

            }
        });

    }

    //posting a status
    private void createStatus(final String status, final String user_id){
        String tag_string_req = " req_register";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.STATUS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    //
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("user_id:", "User id is: " + user_id);
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("status", status);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
    }

    //getting all the status and putting them in a list
    private void getStatus(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppConfig.GET_STATUS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int i = 0; i< jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String username = jsonObject.getString("name");
                                String userStatus = jsonObject.getString("status");
                                String statusID = jsonObject.getString("status_id");
                                String likes = jsonObject.getString("likes");
                                String createdDateStatus = jsonObject.getString("created_date");
                                statusData.add(new StatusData(username, userStatus, statusID, likes, createdDateStatus));
                                progressBar.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    //like button
    private void likeButton(final String statusID){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.GET_LIKES_COUNTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        View view = getLayoutInflater().inflate(R.layout.item_status_list, null);
                        TextView likesCounter = (TextView)view.findViewById(R.id.like_counter);
                        TextView likesText = (TextView)view.findViewById(R.id.user_like);


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean error = jsonObject.getBoolean("error");

                            if(!error){
                                String likes = jsonObject.getString("likes");

                                Toast.makeText(getApplicationContext(), "Total likes: " + likes, Toast.LENGTH_SHORT).show();

                                likesCounter.setText(likes);
                                likesText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("status_id", statusID);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest, "url_request");
    }

    //check if the status is liked
    public void isLiked(){

    }

    //unfocus the status section on touching outside the region
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (statusET.isFocused()) {
                Rect outRect = new Rect();
                statusET.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    statusET.clearFocus();
                    //
                    // Hide keyboard
                    //
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if (id == R.id.log_out){
            sessionManager.setLogin(false);
            sqLiteHandler.deleteUsers();
            Toast.makeText(getApplicationContext(), "Logout successful", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        return false;
    }
}
