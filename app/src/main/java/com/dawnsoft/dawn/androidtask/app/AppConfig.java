package com.dawnsoft.dawn.androidtask.app;

/**
 * Created by dawn on 3/31/2018.
 */

public class AppConfig {

    public static final String REGISTER_URL = "http://192.168.1.7/android_task/include/register";
    public static final String LOGIN_URL = "http://192.168.1.7/android_task/include/login";
    public static final String STATUS_URL = "http://192.168.1.7/android_task/include/status";
    public static final String GET_STATUS_URL = "http://192.168.1.7/android_task/include/getstatus";
    public static final String GET_LIKES_COUNTER = "http://192.168.1.7/android_task/include/likescounter";
}
