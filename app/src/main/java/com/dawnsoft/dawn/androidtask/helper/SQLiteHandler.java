package com.dawnsoft.dawn.androidtask.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by dawn on 3/31/2018.
 */

public class SQLiteHandler extends SQLiteOpenHelper{

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    //session manager
    SessionManager sessionManager;

    private static final int DB_VERSION = 7;
    private static final String DB_NAME = "android_api";
    private static final String TABLE_USER = "user_table";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_UID = "user_id";
    private static final String KEY_IS_ACTIVE = "is_active";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_CREATED_DATE = "created_date";

    public SQLiteHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        sessionManager = new SessionManager(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NAME + " TEXT NOT NULL, "
                + KEY_USERNAME + " TEXT NOT NULL, "
                + KEY_UID + " TEXT NOT NULL, "
                + KEY_IS_ACTIVE + " BOOLEAN, "
                + KEY_CREATED_DATE + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }

    public void addUsers(String name, String username, String userId, String created_date){
        SQLiteDatabase db = this.getReadableDatabase();
        boolean isActive = sessionManager.isLoggedIn();
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, name);
        cv.put(KEY_USERNAME, username);
        cv.put(KEY_UID, userId);
        cv.put(KEY_IS_ACTIVE, isActive);
        cv.put(KEY_CREATED_DATE, created_date);

        //inserting row;
        long id = db.insert(TABLE_USER, null, cv);
        db.close();
        Log.d(TAG, "New user inserted into sqlite: " + id);

    }

    //getting user data from database
    public HashMap<String, String> getUserDetails (){
        HashMap<String, String> user = new HashMap<>();
        String SELECT_QUERY = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0){
            user.put("name", cursor.getString(1));
            user.put("username", cursor.getString(2));
            user.put("user_id", cursor.getString(3));
            user.put("is_active", cursor.getString(4));
            user.put("created_date", cursor.getString(5));
        }
        cursor.close();
        db.close();

        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());
        return user;
    }

    //getting userid from database
    public int getUserId(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_UID + " FROM " + TABLE_USER;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        int userId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_UID)));

        cursor.close();
        db.close();

        return userId;
    }

    //delete user
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
}
